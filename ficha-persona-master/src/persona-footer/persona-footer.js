import { LitElement, html } from 'lit-element';

class PersonaFooter extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <h5>@Persona App 2020</h5>
            <!-- Enlace Bootstrap -->
            <div class="card h-100">
                <img src="${this.photo.src}" alt="${this.photo.alt}" height="50" width="50" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">${this.name}</h5>
                        <p class="card-text">${this.profile}</p>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">${this.yearsInCompany} años en la empresa</li>
                        </ul>
                    </div>
                <div class="card-footer">
                    <button class="btn btn-danger col-5"><strong>X</strong></button>
                </div>				
            </div>

        `;
    }
}

customElements.define('persona-footer', PersonaFooter)